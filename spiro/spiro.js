// "spiro", the Scouted Portal Info ROster

var Spiro = {

/* utility {{{ */
	
// wrapper for specifying coordinates as (latitude, longitude)
// TODO: does leaflet already have this?
latLon: function(lat, lon) {
	return [lon, lat];
},

// geojson Feature/Point template for plotting portals
emptyGeoJsonPoint: function() {
	return {
		"type": "Feature", "properties": {}, "geometry": { "coordinates": null, "type": "Point" }
	};
},
	
/* }}} */

/* normalising portal data {{{ */

// standard or internal values for properties
// a kind of 'proto owlwing' of sorts
fillerWing: function() {
	return {
		keys: {
			"name": "portal_name",
			"comment": "portal_comment",
			"portaltype_pkmn": "portal_type:pkmn",
			"portaltype_hp": "portal_type:hp"
		},
		values: {
			base_lat: [ undefined, 'no' ], base_lon: [ undefined, 'no' ],
			portal_lat: [ undefined, 'no' ], portal_lon: [ undefined, 'no' ],
			portaltype_hp: [ null, 'none' ]
		}
	};
},

// expand Simple representation of portal out into standard geojson
expandSimplePortal: function(point) {
	var p = point, expanded = Spiro.emptyGeoJsonPoint(), key, valueList, i;
	const fillers = Spiro.fillerWing();
	
	// there are a few values which are simply used as fillers
	// and sometimes variations of keys
	// if found, remove/normalise them
	for (key in fillers.keys) {
		if (point[key] !== undefined) {
			point[ fillers.keys[key] ] = point[key];
			delete point[key];
	}}
	for (key in fillers.values) {
		valueList = fillers.values[key];
		// index 0 in the array is the standard value, so skip that
		for (i = 1; i < valueList.length; i++) {
			// if key has one of the nonstandard values, make it the standard one
			if (point[key] === valueList[i]) {
				point[key] = valueList[0];
				// if this causes the key to become undefined, remove it
				if (point[key] === undefined) {
					delete point[key];
	}}}}
	
	// copy over simple properties of point
	for (key in point) {
		expanded.properties[key] = point[key];
	}
	
	// still figuring out what's the best thing to do with a wrongly-placed portal
	// this plots the portal at its game coords
	if (p.portal_lat !== undefined && p.portal_lon !== undefined) {
		expanded.geometry.coordinates = Spiro.latLon(p.portal_lat, p.portal_lon);
	}
	else if (p.base_lat !== undefined && p.base_lon !== undefined) {
		expanded.geometry.coordinates = Spiro.latLon(p.base_lat, p.base_lon);
	}
	
	//onsole.log("EXPAND", expanded);
	return expanded;
	
	/* example:
	{
		"type": "Feature",
		"properties": { "name": "Feature name", "portal_type:pkmn": "portal" },
		"geometry": { "coordinates": [-71.1322511,46.8451252], "type": "Point" }
	}
	*/
},

/* }}} */

// retrieve json object from template tag
extractPortalJSON: function(templateTag) {
	const NO_EXPAND = false;
	var jsonText = Spiro.extractTemplateText(templateTag);
	var jsonData = JSON.parse(jsonText);
	
	// index is usually deleted when saving json, so rebuild it
	if (jsonData.index === undefined) {
		jsonData.index = {};
	}
	if (jsonData.startArea !== undefined) {
		jsonData.index.site_start_area = jsonData.startArea;
		jsonData.list.push(jsonData.index.site_start_area);
		delete jsonData.startArea;
	}
	jsonData = Spiro.geoJSONIndexed(jsonData.list, NO_EXPAND);
	console.log("DATA", jsonData);
	
	return jsonData;
},

// turn an array of portal records into a structured list of geojson
geoJSONIndexed: function(portals, expandPoints = true) {
	var expanded, points = [], index = {}, hasStartArea = false, startArea;
	
	for (i = 0; i < portals.length; i++) {
		var key, point = portals[i];
		
		// if a start area record is found, file it under site_start_area and don't expand it
		if (hasStartArea !== true &&
		   (point.site_start_area !== undefined || point.properties.site_start_area !== undefined)) {
			if (point.properties !== undefined) {
				point = point.properties;
			}
			startArea = point;
			hasStartArea = true;
		}
		else {
			// convert portal to standard geojson,
			// unless unserialising already-converted data from template tag.
			if (expandPoints === true) {
				point = Spiro.expandSimplePortal(point);
			}
			
			// portals are indexed by strings so that even if they lack an osm node you can fill in an arbitrary string to reference them by
			if (point.properties !== undefined) {
				key = point.properties.node + "";
			}
			
			// add point to point list
			points.push(point);
		}
		
		// if this id is used multiple times, remove it from index
		// otherwise file portal under its node id
		if (index[key] !== undefined) {
			delete index[key];
		}
		else if (key !== undefined) {
			index[key] = point;
		}
	}
	
	return {
		list: points, index: index, startArea: startArea
	};
},

/* interfacing with Leaflet + webpage in browser {{{ */

// retrieve json text from template tag
extractTemplateText: function(templateTag) {
	var content = templateTag.content.cloneNode(true);
	var scriptTag = content.firstElementChild;
	return scriptTag.text;
},

// place geojson point as marker
geoJsonMarker: function(geoJsonPoint, latlng) {
	//onsole.log("PLOTTING POINT", geoJsonPoint, latlng);
	return L.marker(latlng);
},

// plot individual portals on map - out of date, currently only testing geojson
plotPortals: function(portals, map) {
	var p, realPortal;
	
	// for each item in portals, put a marker on the map
	for (i = 0; i < portals.length; i++) {
		p = portals[i];
		L.marker({lat: p.lat, lon: p.lon, icon: ICON_PORTAL}).bindPopup(p.title).addTo(map);
		
		// if separate portal coords specified, also draw portal where it actually is
		// if value is undefined, make it an empty set to skip it
		if (p.portal_coords == undefined) {
			p.portal_coords = {};
		}
		// otherwise plot each coordinate
		for (j in p.portal_coords) {
			realPortal = p.portal_coords[j];
			L.marker({lat: realPortal[0], lon: realPortal[1], icon: ICON_PORTAL}).bindPopup(p.title + " [actual portal]").addTo(map);
		}
	}
},

// move map to a geojson point object such as a portal
snapToPortal: function(map, geoJsonPortal) {
	var portal = geoJsonPortal, coords;
	
	// all portals should be Points, but if not, that situation would be handled here
	if (portal.geometry.type == "Point") {
		coords = portal.geometry.coordinates;
	}
	else {
		return;
	}
	
	// in geojson coords are stored as lonlat, however Leaflet has a nice labelled object to get rid of the ordering issue
	var latlon = L.latLng(coords[1], coords[0]);
	map.setView(latlon, map._zoom);
},

// read initial settings to move map to start position
initMap: function(initial) {
	var map = L.map('map', { attributionControl: false });
	
	// move map to initial coordinates and zoom
	map.setView({ lat: initial.lat, lon: initial.lon }, initial.zoom);
	
	// set attribution
	L.control.attribution({ prefix: 'Spiro / <a href="https://leafletjs.com/">Leaflet</a>' }).addTo(map);
	
	// add the OpenStreetMap tiles
	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		maxZoom: 19,
		attribution: '&copy; <a href="https://openstreetmap.org/copyright">OpenStreetMap contributors</a>'
	}).addTo(map);
	
	// show the scale bar on the lower left corner
	L.control.scale().addTo(map);
	
	return map;
},

/* }}} */

/* debug / misc {{{ */

// error constructor for portal-related errors
PortalException: function(message, portal) {
  this.message = message;
  this.portal = portal;
  this.toString = function() {
    return this.message + JSON.stringify(this.portal);
  };
},

yesVirginia: function(value) { // joke I used while debugging
	if (value !== undefined) {
		console.log("Yes virginia, there IS a value", value);
	}
	else {
		console.log("No virginia, there is NO value", value);
	}
},

iHateCommas: true  // <- so every real method will have a comma

/* }}} */

};

delete Spiro.iHateCommas;

// if on nodejs, export commonjs module
if (typeof module !== 'undefined') {
	module.exports = Spiro;
}
// otherwise an html page can simply use the global variable:
// Spiro.yesVirginia(null)  // or:
// var spiro = Spiro;
