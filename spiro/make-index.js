// make-index.js - script to generate an index file
// i initially wanted to hardcode the index file to open a nearby 'portals.json'.
// "that can't be too much of a pain, can it?" oh, browsers are a trip and it absolutely can.
// so, I decided this would instead precompile the portals into the index file.

const fs = require('fs'), path = require('path'), crypto = require('crypto'),
      spiro = require('spiro');

// open html template and geojson portals file asynchronously, then continue the function above
var openFiles = function(dataFilename, templateFilename, callback) {
	// attempt to open data file
	fs.readFile(dataFilename, function (err, data) {
		if (err) { throw err; }
		var portalData = data.toString();
		
		// attempt to open template file
		fs.readFile(templateFilename, function (err, data) {
			if (err) { throw err; }
			var templateData = data.toString();
			
			callback(templateData, portalData);
		});
	});
};

var main = function() {
	const scriptDir = path.dirname( process.argv[1] );
	var templateFilename = path.join(scriptDir, './index-template.html');
	var dataFilename = process.argv[2];
	var outputTo = path.join(scriptDir, '../site_index.html');
	
	const SEARCH_STRING = "\t<template id=\"portals_template\"></template>";
	
	if (dataFilename === undefined) {
		dataFilename = path.join(scriptDir, '../site_portals/portals.json');
	}
	
	// open files asynchronously and then continue
	openFiles(dataFilename, templateFilename, function(template, portals) {
		// for better performance (?), cheat by splitting template instead of .replace() or DOM parsing
		template = template.split(SEARCH_STRING);
		
		// insert portals
		var result = template[0] +
		"\t<template id=\"portals_template\"><script type=\"application/json\">\n" +
		"\t\t" + portals + "\n" +
		"\t</script></template>" +
		template[1];
		
		fs.writeFile(outputTo, result, (err) => {
			if (err) throw err;
			console.log("Saved instance homepage to ", outputTo);
		});
	});
};

main();
