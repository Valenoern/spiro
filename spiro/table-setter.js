// table-setter.js - a script to prepare data tables in csv or ytp format as complete geojson
// this should be run on nodejs as:  node table-setter.js <data-table> <output-dir>

const fs = require('fs'), path = require('path'), crypto = require('crypto'),
      ytp = require('spiro-ytp'), spiro = require('spiro');

// {{{
var tableToJson = function(textData) {
	var records, indexed, stringified;
	records = ytp.ytpToRecords(textData);
	indexed = spiro.geoJSONIndexed(records);
	
	return indexed;
};

// construct query statement to get node information from OSM overpass
// not actually sending these to the net yet
var overpassQuery = function(jsonData) {
	// only look through portals that 'probably' have node IDs
	var portals = jsonData.index, idNumber, validNodes = [], validWays = [], validRelations = [];
	
	for (i in portals) {
		idNumber = i * 1;
		
		// if node is usable add it to one of the valid nodes lists
		if (!isNaN(idNumber) && portals[i].properties !== undefined) {
			var nodeType = portals[i].properties.node_type;
			
			if (nodeType == 'way') { validWays.push(i); }
			else if (nodeType == 'rel') { validRelations.push(i); }
			// assume most points are Nodes to be surer what the code will return
			else { validNodes.push(i); }
		}
		// if node is not usable print a warning
		else {
			var name = i + '';;
			if (name.length == 0) { name = portals[i].name; }
			console.error("Portal \""+ i +"\" does not have an ID number, and was not tagged");
		}
	}
	
	// if no valid osm points were found, quit
	if ((validNodes.length + validWays.length + validRelations.length) < 1) {
		return null;
	}
	
	// construct each section of the query
	var nodeString = '', wayString = '', relString = '';
	if (validNodes.length > 0) {
		nodeString = " node(id:" + validNodes.join(',') + ");";
	}
	if (validWays.length > 0) {
		wayString = "  way(id:" + validWays.join(',') + ");";
	}
	if (validRelations.length > 0) {
		relString = "  rel(id:" + validRelations.join(',') + ");";
	}
	
	// this query returns the 'union of the sets' of the node, way, and relation selectors,
	// in json format, containing the centre points of each object
	var query = "[out:json]; ( " + nodeString + wayString + relString + "  ); out center;";
	
	// example query:
	// [out:json]; (  node(id:2953135622);  way(id:291806795);  ); out center;
	// this retrieves the pavillion and viewpoint in Quebec City i keep using to demo spiro.
	
	return query;
};

// fetch osm query result from disk
var loadOsmCache = function(osmCacheFile, callback) {
	fs.readFile(osmCacheFile, function (err, data) {
		if (err) { throw err; }
		data = data.toString();
		var jsonData = JSON.parse(data);
		callback(jsonData);
	});
};

// remove circular structures etc from json and flatten to string
var flattenJsonData = function(jsonData) {
	// 'index' is the main culprit for circular structures
	// however everything in it should already be in 'list' or 'startArea'
	if (jsonData.index !== undefined) {
		delete jsonData.index;
	}
	
	return JSON.stringify(jsonData);
};

// save json file and the sha256 hash of its original data
// LATER: when allowing multiple text data files, save multiple hashes
var saveJson = function(textData, jsonData, outputDir) {
	var outputJson = path.join(outputDir, './portals.json');
	var outputHash = path.join(outputDir, './portals.json.sha256');
	
	//stringified = JSON.stringify(indexed);
	
	// save hash of initial portal data to avoid unnecessary regenerating
	// this is currently not used yet
	const hash = crypto.createHash('sha256');
	hash.update(textData);
	var textHash = hash.digest('hex');
	
	// prepare json object to be saved
	jsonData = flattenJsonData(jsonData);
	
	// it doesn't especially matter what order the files are saved in,
	// so we don't really have to wait for one to finish
	
	fs.writeFile(outputHash, textHash, (err) => {
		if (err) throw err;
		console.log("Saved checksum:", textHash);
	});
	
	fs.writeFile(outputJson, jsonData, (err) => {
		if (err) throw err;
		console.log("Saved json portal data to ", outputJson);
	});
};
// }}}

var osmTagPortals = function(portals, osmData) {
	var i, tag, nodes = osmData.elements, point, portal, portalCoords;
	
	for (i = 0; i < nodes.length; i++) {
		point = nodes[i];
		portal = portals.index[ point.id + '' ];
		
		// if osm point is not in portal list discard it
		// LATER: add more error handlers if there are any problems
		if (portal === undefined) { continue; }
		
		// get portal coordinates from osm feature
		if (point.type == 'way' || point.type == 'relation') {
			portalCoords = spiro.latLon( point.center.lat, point.center.lon );
			//onsole.log("AREA PORTAL", portalCoords);
		}
		else if (point.type == 'node') {
			portalCoords = spiro.latLon(point.lat, point.lon);
			//onsole.log("NODE PORTAL", portalCoords);
		}
		else {
			console.error('Unknown node type "'+ point.type +'"');
		}
		
		// if coordinates found, give them to portal
		if (portalCoords !== undefined) {
			portal.geometry.coordinates = portalCoords;
			// remove base_lat & base_lon as they aren't being used to plot point
			delete portal.properties.base_lat;
			delete portal.properties.base_lon;
		}
		
		// switch portal to numeric id
		portal.properties.node = point.id;
		
		// add each osm tag to point's geojson properties
		for (tag in point.tags) {
			// if tag already exists, prefix spiro version of tag in order to not clobber it
			if (portal.properties[tag] !== undefined) {
				portal.properties["spiro:" + tag] = portal.properties[tag];
			}
			portal.properties[tag] = point.tags[tag];
		}
		
		// since javascript objects are only references, each portal should already be updated
	}
	
	return portals;
};

// currently just outputs query to terminal
var loadPortalsThenQuery = function(portalFile, osmFile, outputDir) {
	fs.readFile(portalFile, function (err, data) {
		if (err) { throw err; }
		var textData = data.toString();
		var jsonData = tableToJson(textData);
		
		var query = overpassQuery(jsonData);
		console.log(query);
		
		// LATER: when queries are sent to osm overpass,
		// don't send a new query unless hash changed or user forces an update
	});
};

var loadPortalsThenCached = function(portalFile, osmFile, outputDir) {
	fs.readFile(portalFile, function (err, data) {
		if (err) { throw err; }
		var textData = data.toString();
		var jsonData = tableToJson(textData);
		
		// open cached overpass file
		loadOsmCache(osmFile, function(osmData) {
			// when cache found, apply it to portals
			var taggedPortals = osmTagPortals(jsonData, osmData);
			
			// write tagged portals to disk
			saveJson(textData, jsonData, outputDir);
		});
	});
};

var main = function() {
	// $0 is node and the script name, $1 is the text data table, $2 is the directory to output to
	var filename = process.argv[2], outputTo = process.argv[3];
	const scriptDir = path.dirname( process.argv[1] );
	
	if (filename === undefined) {
		console.error("Please specify a filename");
		return;
	}
	if (outputTo === undefined) {
		// if no output directory given, output inside the spiro source dir
		outputTo = path.join(scriptDir, '../site_portals/');
	}
	
	var osmCacheFile = path.join(scriptDir, '../site_osm_cache.json');
	
	// attempt to open and parse the portals file
	loadPortalsThenCached(filename, osmCacheFile, outputTo);
};

main();
