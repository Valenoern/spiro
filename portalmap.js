// ""require"" spiro. unfortunately browsers can't actually use require()
const spiro = Spiro;
// the leaflet library "L" is already loaded from leafletjs.com

var startArea, portals, siteStartArea, sitePortals, i;

// not currently working, default icon appears instead
var ICON_PORTAL = L.icon({
    iconUrl: './mark-location-symbolic.svg',
    iconSize: [38, 95],
    iconAnchor: [22, 94],
    popupAnchor: [-3, -76],
    //shadowUrl: 'my-icon-shadow.png',
    shadowSize: [68, 95],
    shadowAnchor: [22, 94]
});

// initial settings for map - default ones {{{
// these can be replaced with data for a different location in ./site_portals/portals.json
startArea = {
	// if no specific coords supplied the map starts at Quebec City. because why not
	portal: 2953135622
	, lat: 0, lon: 0  // the portal object holds the coordinates
	, zoom: 17
};
portals = [
	{ name: "pavilion [check title]", node: 2953135622, portaltype_pkmn: "basic?",
		base_lat: 46.8451252, base_lon: -71.1322511
	}
];
// }}}

/* TODO / LATER:
- disambiguate portal ids by node type - n74835983475, w875874854 etc
*/

// at one point I had such a baffling error plotting portals I had to make this
var debugPortalProblem = function(portals, map) {
	var i, portal, error = spiro.PortalException;
	console.error("Problem with portal data.");
	console.warn("Troubleshooting portal objects one by one:");
	
	// portals.length
	try {
		for (i = 0; i < portals.length; i++) {
			console.info("Portal", i, portal);
			portal = portals[i];
			if (portal.properties.site_start_area !== undefined) {
				throw new error("startArea found in portals list. it should only be in 'index'", portal);
			}
			else if (portal.geometry.coordinates === null) {
				throw new error("Portal has no coordinates", portal);
			}
			//else {
			//	L.geoJSON(portal, { pointToLayer: spiro.geoJsonMarker }).addTo(map);
			//}
	}}
	catch (e) {
		console.error("Problem portal found.");
		console.error(e.message, e.portal);
	}
};

var testMain = function(startArea, portals, siteStartArea) {
	var initial = startArea, portals = portals, currentPortals;
	
	var portalsTag = document.querySelector('template#portals_template');
	var portalData = spiro.extractPortalJSON(portalsTag);
	
	// LATER: add back in handler for if there's no portal data for some reason
	
	// initialise leaflet map drawing within #map div
	var map = spiro.initMap(initial);
	
	var siteStartArea = portalData.startArea;
	initial = siteStartArea;
	portals = portalData;
	
	console.log('startarea', initial);
	
	// plot portal points
	currentPortals = portals;
	
	try {
		L.geoJSON(currentPortals.list, { pointToLayer: spiro.geoJsonMarker }).addTo(map);
	}
	catch (e) { debugPortalProblem(currentPortals.list); }
	
	// if initial settings specify a portal to jump to,
	// go to that portal
	if (initial.portal !== undefined) {
		var portalNode = initial.portal + "";
		spiro.snapToPortal(map, currentPortals.index[ portalNode ]);
	}
};

testMain(startArea, portals, siteStartArea);
