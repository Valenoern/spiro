var YourTextPlaylists = {

// convert csv file to array of Simple map points
csvToRecords: function(csv) {
	var headers, rows = csv.split("\n"), row, rowObj, i;
	
	for (i = 0; i < rows.length; i++) {
		row = rows[i] = rows[i].split(',');
		if (i == 0) {
			headers = row;
		}
		else {
			rowObj = {};
			for (j = 0; j < row.length; j++) {
				rowObj[ headers[j] ] = row[j];
			}
		}
		//onsole.log("ROW OBJ", rowObj);
	}
	
	return rows;
},

// return true if ytp row is not a data record
isIgnoredRow: function(row) {
	var isEmpty = false, isComment = false;
	
	if (row == "") {
		isEmpty = true;
	}
	
	if (row[0] == "#" && /^#(COLUMNS)/.test(row) !== true) {
		isComment = true;
	}
	
	if (isEmpty || isComment) {
		return true;
	}
	return false;
},

rowToCells: function(row) {
	var quotedValue = false, rowCells = [], cell = "", chr, j;
	
	for (j = 0; j < row.length; j++) {
		chr = row[j];
		
		if (chr == '"') {
			quotedValue = !quotedValue;
		}
		else if (quotedValue == true || /\s/.test(chr) == false) {
			cell += row[j];
		}
		
		// if not inside a quoted value and a space found,
		// advance to the next cell
		if ((quotedValue == false && /\s/.test(chr)) || j == (row.length - 1)) {
			rowCells.push(cell);
			cell = "";
		}
	}
	
	return rowCells;
},

cellsToObject: function(row, columns) {
	var minLength = Math.min(row.length, columns.length), rowObject = {}, i;
	for (i = 0; i < minLength; i++) {
		rowObject[ columns[i] ] = row[i];
	}
	return rowObject;
},

// convert ytp file (space separated data table) to array of Simple map points
ytpToRecords: function(ytpString) {
	const ytp = YourTextPlaylists;
	var columns, rows, i, result = [];
	
	rows = ytpString.split("\n");
	
	// parse rows line by line
	for (i = 0; i < rows.length; i++) {
		var row = rows[i], rowCells, rowObject;
		
		// discard empty rows and comment rows
		if (ytp.isIgnoredRow(row)) { continue; }
		
		// flatten spaces to make parsing simpler
		row = row.replace(/(\s)\s+/g, '$1');
		// separate row at delimiters
		rowCells = ytp.rowToCells(row);
		
		if (rowCells[0] == "#COLUMNS") {
			columns = rowCells.slice(1);
		}
		else {
			// if columns were supplied, label fields using columns
			if (columns !== undefined) {
				rowObject = ytp.cellsToObject(rowCells, columns);
				result.push(rowObject);
			}
			// otherwise simply return an unlabelled array
			else {
				result.push(rowCells);
			}
		}
	}
	
	//onsole.log(JSON.stringify(result, null, 2));
	return result;
},

iHateCommas: true
};

delete YourTextPlaylists.iHateCommas;

// if on nodejs, export commonjs module
if (typeof module !== 'undefined') {
	module.exports = YourTextPlaylists;
}
// otherwise an html page can simply use the global variable
// var ytp = YourTextPlaylists;
// ytp.ytpToRecords(textData);
