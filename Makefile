build: clean node_modules

node_modules:
	mkdir -p node_modules
	cp -a spiro node_modules/spiro
	cp -a ytp node_modules/spiro-ytp

portals:
	rm -rf site_portals
	mkdir -p site_portals
	node ./spiro/table-setter.js ./site_portals.ytp

homepage: portals
	node ./spiro/make-index.js
	
clean:
	rm -rf node_modules site_portals site_index.html
